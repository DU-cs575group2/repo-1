<?php
	require_once '../config.php';
	
	//Start session
	session_start();

	if(isset($_POST) && count($_POST) > 0) {
		//Array to store validation errors
		$errmsg_arr = array();
		
		//Validation error flag
		$errflag = false;
		
		//Connect to mysql server
		$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);

		if(!$link) {
			die('Failed to connect to server: ' . mysqli_error());
		}
		
		//Select database
		$db = mysqli_select_db($link, DB_DATABASE);
		if(!$db) {
			die("Unable to select database");
		}
		
		//Function to sanitize values received from the form. Prevents SQL injection
		function clean($str) {
			$str = @trim($str);
			if(get_magic_quotes_gpc()) {
				$str = stripslashes($str);
			}
			return $str;
		}


		if(!$errflag) {
			$user_id = $_SESSION['SESS_USER_ID'];
			$qry="UPDATE `users` SET `course1`= '".$_POST['course1']."',`course2`='".$_POST['course2']."',  `course3` = '".$_POST['course3']."' WHERE `users`.`id` = ".$user_id.";";
					//	echo $qry; exit();
			/*
			*/$result = mysqli_query($link, $qry);
	
			//Check whether the query was successful or not
			if(!$result) {
				die("Query failed: ".mysqli_error($link));
				exit();
			}
			else {
				header('Content-type: application/javascript');
				echo json_encode(array('done' => 1));
			}
		}
		else {
			header('Content-type: application/javascript');
			echo json_encode(array('errors' => $errmsg_arr));
		}
		exit();

	}
		
