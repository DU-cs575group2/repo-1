<?php

require '../config.php';
if(isset($_GET) && count($_GET) > 0) {
		//Array to store validation errors
	$errmsg_arr = array();

		//Validation error flag
	$errflag = false;

		//Connect to mysql server
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);

	if(!$link) {
		die('Failed to connect to server: ' . mysqli_error());
	}

		//Select database
	$db = mysqli_select_db($link, DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}

		//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
	}

		//Sanitize the POST values

	$email = clean($_GET['email']);
	$password = clean($_GET['password']);

	if($email == '') {
		$errmsg_arr[] = 'Email missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}

	if(!$errflag) {
		$qry="SELECT * FROM users WHERE email='$email' AND (password='$password' OR password='".md5($password)."')";
		$result=mysqli_query($link, $qry);
		$count=mysqli_num_rows($result);


			//Check whether the query was successful or not
		if($result) {
			//Login Successful
			if($count==1) {
				
				session_start();
				session_regenerate_id();
				$member = mysqli_fetch_assoc($result);
				$_SESSION['SESS_USER_ID'] = $member['id'];
				$_SESSION['SESS_EMAIL'] = $member['email'];
				$_SESSION['SESS_NAME'] = $member['first_name'];
				$_SESSION['SESS_ADMIN'] = $member['is_admin'];
				
				if($member['is_admin']==1) {
					$redirectURL = '../admin-dashboard.php';
				}
				elseif ($member['is_admin']==0) {
						$redirectURL = '../home.php';
				}

				// $redirectURL = $member['is_admin'] ? 'dashboard.php' : 'profile-detail-form.php';
				// $r = $member['first_login'] ? 'home.php' : 'profile-detail-form.php';
				session_write_close();
				// echo json_encode(array('done' => 1, 'redirectURL' => $redirectURL));
				header("Location:".$redirectURL);
				exit();
			} else {
						//Login failed
				// $errmsg = 'Incorrect username or password. Please try again.';
				$errflag = true;
				header("Location: ../login.php?errflag=".$errflag);
				// header('Content-type: application/javascript');
				// echo json_encode(array('errors' => $errmsg_arr));
			}	
		}
	} else {
		die("Query failed: ".mysqli_error($link));
	}
}
else {
	header('Content-type: application/javascript');
	// echo json_encode(array('errors' => 1, 'data' => $errmsg_arr));
}
?>