<?php
	require_once '../config.php';
	
	//Start session
	session_start();

	if(isset($_POST) && count($_POST) > 0) {
		//Array to store validation errors
		$errmsg_arr = array();
		
		//Validation error flag
		$errflag = false;
		
		//Connect to mysql server
		$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);

		if(!$link) {
			die('Failed to connect to server: ' . mysqli_error());
		}
		
		//Select database
		$db = mysqli_select_db($link, DB_DATABASE);
		if(!$db) {
			die("Unable to select database");
		}
		
		//Function to sanitize values received from the form. Prevents SQL injection
		function clean($str) {
			$str = @trim($str);
			if(get_magic_quotes_gpc()) {
				$str = stripslashes($str);
			}
			return $str;
		}
		
		if(isset($_FILES["files1"]))
		{

				$target_file= '../uploads/' . basename($_FILES['files1']['name']);
				//echo $target_file;exit;
			       $fname = pathinfo($_FILES['files1']['name'], PATHINFO_FILENAME);
			        $extension = pathinfo($_FILES['files1']['name'], PATHINFO_EXTENSION);
			      
			        $file_1 = $fname.'.' . $extension;
			    //    echo $file_1;exit;
					$uploadOk = 1;
				//$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				
				
				// Check if file already exists
				if (file_exists($target_file)) {
				   $fname = $fname.rand();
	       			$file_1 = $fname.'.'. $extension;
				}
				//echo $file_1; exit;
				// Check file size
				if ($_FILES["files"]["size"] > 500000) {
				    echo "Sorry, your file is too large.";
				    $uploadOk = 0;
				}
				
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
				    echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
				} else {
					
					
				    if (move_uploaded_file($_FILES["files1"]["tmp_name"], '../uploads/'. $file_1)) {
				        //echo "The file ".$file_1. " has been uploaded.";
				    } else {
				        header('Content-type: application/javascript');
						echo json_encode(array('errors' => 'The file couldnt be uploaded'));
				    }
				}

		}
		else{
			echo "no file";exit;
		}

		if(!$errflag) {

			$user_id = $_SESSION['SESS_USER_ID'];
			$qry = "INSERT INTO `notes` (`user_id`, `title`, `description`,`course`, `created_at`, `updated_at`,`file`)
							VALUES (".$user_id.", '".$_POST['note_title']."', '".$_POST['note_description']."','".$_POST['course']."', '".date('Y-m-d G:i:s')."', '".date('Y-m-d G:i:s')."','".$file_1."');";
			$result = mysqli_query($link, $qry);
	
			//Check whether the query was successful or not
			if(!$result) {
				die("Query failed: ".mysqli_error($link));
				exit();
			}
			else {
				header('Content-type: application/javascript');
				echo json_encode(array('done' => 1));
			}
		}
		else {
			header('Content-type: application/javascript');
			echo json_encode(array('errors' => $errmsg_arr));
		}
		exit();

	}
		
