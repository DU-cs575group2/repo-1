<?php
	require_once 'config.php';
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	//Connect to mysql server
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);

	if(!$link) {
		die('Failed to connect to server: ' . mysqli_error());
	}
	
	//Select database
	$db = mysqli_select_db($link, DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}

	$qry="SELECT * FROM notes WHERE (`course`='".$course1."' OR `course`='".$course2."' OR `course`='".$course3."') AND `flag`='1' ORDER BY `id` DESC";
	
	$result=mysqli_query($link, $qry);
	$news = array();

	//Check whether the query was successful or not
	if($result) {
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {             
		    $news[] = $row;
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			header('Content-type: application/javascript');
			echo json_encode(array('data' => $news));
			exit();
		}
	}
	else {
		header('Content-type: application/javascript');
		echo json_encode(array('error' => 1));		
	}

?>