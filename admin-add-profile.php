<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard - Add News</title>
<style>
.alert{
    margin:10px 0 0px 0;
}
</style>
</head>
<?php include 'navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-offset-3 col-md-6">
                <h4 class="page-header">Add Profile</h4>
                <form onsubmit="return false;" class="add-profile-form">
                    <div class="form-group">
                        <label class="control-label">First Name</label>
                        <div class="controls">
                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name">
                             <div class="error" id="fnameError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Last Name</label>
                        <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name">
                         <div class="error" id="lnameError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email ID</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Last Name">
                         <div class="error" id="emailError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                         <div class="error" id="email1Error"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                      <div class="form-group">
                        <label class="form-label">Password</label>
                        <input type="password" id="password1" name="password1" class="form-control" >
                         <div class="error" id="pass1Error"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Confirm Password</label>
                        <input type="password" id="password2" name="password2" class="form-control" >
                         <div class="error" id="pass2Error"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                         <div class="error" id="pass3Error"><div class="alert alert-danger" role="alert">The Passwords do not match</div></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Contact Number</label>
                        <div class="controls">
                            <input type="text" name="contact_number" id="contact_number" class="form-control" placeholder="Contact Number">
                             <div class="error" id="contactError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label">University ID</label>
                        <div class="controls">
                            <input type="text" name="univ_id" id="univ_id" class="form-control" placeholder="University ID">
                             <div class="error" id="univError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <div class="controls">
                         <textarea name="address" id="address" class="form-control"></textarea>
                          <div class="error" id="addressError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label">Gender</label>
                        <div class="controls">
                           <select class="form-control" name="gender" id="gender">
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                              <option value="other">Other</option>  
                            </select>
                             <div class="error" id="genderError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Date of Birth</label>
                        <input type="date" name="birthday" id="birthday" class="form-control" placeholder="Date of Birth">
                         <div class="error" id="birthdayError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Profile Type</label>
                         <select class="form-control" name="profile_type" id="profile_type">
                              <option value="student">Student</option>
                              <option value="faculty">Faculty</option>
                              <option value="admin">Admin</option>  
                          </select>
                           <div class="error" id="profileError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Department</label>
                        <input type="text" name="department" id="department" class="form-control" placeholder="Department">
                         <div class="error" id="deptError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <p class="text-right"><button class="btn btn-primary" onclick="checkForm();">Add Profile</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
        <script type="text/javascript">
    $( document ).ready(function() {
        hideAllErrors1();

});
    function hideAllErrors1() {
document.getElementById("fnameError").style.display = "none";
document.getElementById("lnameError").style.display = "none";
document.getElementById("emailError").style.display = "none";
document.getElementById("email1Error").style.display = "none";
document.getElementById("pass1Error").style.display = "none";
document.getElementById("pass2Error").style.display = "none";
document.getElementById("pass3Error").style.display = "none";
document.getElementById("contactError").style.display = "none";
document.getElementById("addressError").style.display = "none";
document.getElementById("genderError").style.display = "none";
document.getElementById("birthdayError").style.display = "none";
document.getElementById("profileError").style.display = "none";
document.getElementById("deptError").style.display = "none";
document.getElementById("univError").style.display = "none";
}

function checkForm() {
first_name = document.getElementById("first_name").value;
last_name = document.getElementById("last_name").value;
email = document.getElementById("email").value;
password2 = document.getElementById("password2").value;
password1 = document.getElementById("password1").value;
contact_number = document.getElementById("contact_number").value;
address = document.getElementById("address").value;
gender = document.getElementById("gender").value;
birthday = document.getElementById("birthday").value; 
profile_type = document.getElementById("profile_type").value;
department = document.getElementById("department").value;
univ_id = document.getElementById("univ_id").value;

//address = document.getElementById("address").value; 


if (first_name == "") {

hideAllErrors1();

document.getElementById("fnameError").style.display = "inline";

document.getElementById("first_name").select();

document.getElementById("first_name").focus();

return false;

} 


else if (last_name == "" ) {

hideAllErrors1();

document.getElementById("lnameError").style.display = "inline";

document.getElementById("last_name").select();

document.getElementById("last_name").focus();

return false;

} 


else if (email == "" ) {

hideAllErrors1();

document.getElementById("emailError").style.display = "inline";

document.getElementById("email").select();

document.getElementById("email").focus();

return false;

}

else if (echeck(email)==false) {

hideAllErrors1();

document.getElementById("email1Error").style.display = "inline";

document.getElementById("email").select();

document.getElementById("email").focus();

return false;

}

else if (password1 == "" ) {

hideAllErrors1();

document.getElementById("pass1Error").style.display = "inline";

document.getElementById("password1").select();

document.getElementById("password1").focus();

return false;

}

else if (password2=="") {

hideAllErrors1();

document.getElementById("pass2Error").style.display = "inline";

document.getElementById("password2").select();

document.getElementById("password2").focus();

return false;

}

else if (password2!=password1)
{
  hideAllErrors1();

document.getElementById("pass3Error").style.display = "inline";

document.getElementById("password2").select();

document.getElementById("password2").focus();
document.getElementById("password1").select();

document.getElementById("password1").focus();

return false;  
}

else if (contact_number == "" ) {

hideAllErrors1();

document.getElementById("contactError").style.display = "inline";

document.getElementById("contact_number").select();

document.getElementById("contact_number").focus();

return false;

} 
else if (univ_id == "" ) {

hideAllErrors1();

document.getElementById("unviError").style.display = "inline";

document.getElementById("univ_id").select();

document.getElementById("univ_id").focus();

return false;

}
else if (address == "" ) {

hideAllErrors1();

document.getElementById("addressError").style.display = "inline";

document.getElementById("address").select();

document.getElementById("address").focus();

return false;

}
else if (gender == "" ) {

hideAllErrors1();

document.getElementById("genderError").style.display = "inline";

document.getElementById("contact_number").select();

document.getElementById("contact_number").focus();

return false;

} 

else if (birthday == "" ) {

hideAllErrors1();

document.getElementById("birthdayError").style.display = "inline";

document.getElementById("address").select();

document.getElementById("address").focus();

return false;

}
else if (profile_type == "" ) {

hideAllErrors1();

document.getElementById("profileError").style.display = "inline";

document.getElementById("profile_type").select();

document.getElementById("profile_type").focus();

return false;

} 

else if (department == "" ) {

hideAllErrors1();

document.getElementById("deptError").style.display = "inline";

document.getElementById("department").select();

document.getElementById("department").focus();

return false;

}

senddata();



return true;

}

    function senddata()
    {

       // e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: 'api/add-profile.php',
                    data: $('.add-profile-form').serialize(),
                    success: function(data) {
                        if (data.done){
                            alert('Profile has been created');
                            document.location = 'admin-dashboard.php'
                        }
                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
                });

    }
     function echeck(str) {



    var at="@"

    var dot="."

    var lat=str.indexOf(at)

    var lstr=str.length

    var ldot=str.indexOf(dot)

    if (str.indexOf(at)==-1){

         

         return false

    }



    if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){

         

         return false

    }



    if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){

        

        return false

    }



     if (str.indexOf(at,(lat+1))!=-1){

        

         return false

     }



     if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){

         return false 

     }



     if (str.indexOf(dot,(lat+2))==-1){

        

        return false

     }

    

     if (str.indexOf(" ")!=-1){

        

        return false

     }



     return true          

}
    </script>
</body>

</html>
