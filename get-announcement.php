<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
  <title>Announcements</title>  
  </head>
  <body>
  <div class="container">
    <div class="page-heading">
        <div class="col-md-3">
        <b><h2>ANNOUNCEMENTS</h2></b>
        </div>
        <div class="col-md-3">
        <div class="pull-right"><a class="logout" href="logout.php">Logout</a></div>
        </div>
    </div>
    <div class="container">
      <div class="col-md-6">
      <?php
      include 'api/get-news.php';
      $count=0;
      foreach ($news as $new) {
      if($count<10){
      ?>
      <div class="accordion" id="accordion2">
        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
              <?php echo $new['title']; ?>
            </a>
          </div>
          <div id="" class="accordion-body collapse in">
            <div class="accordion-inner">
              <?php echo $new['description']; ?>
            </div>
          </div>
        </div>
        <br>
      </div>
    <?php
    }
    }
    ?>
    </div>
    <hr>
    <div class="col-xs-12" style="text-align:center;"><a href="home.php">Back to home</a></div>
  </div>
  </body>
  <script>
  $(".collapse").collapse('hide');
  $(".accordion").click(function() {
    $(".collapse").collapse('hide');
    $(this).children().children().last().collapse('toggle');
  });
  </script>
  <style type="text/css">
  .accordion {
    border 1px;
  }
  .accordion{
    background-color:#FFFFF0;
  }

  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
</html>