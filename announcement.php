<?php include 'auth.php'; ?>
<?php include 'header.php'; 
require_once 'config.php';
  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
  $db = mysqli_select_db($con, DB_DATABASE);
?>

  <title>Announcements</title>  
  </head>
  <body>
   <?php include 'navbar.php'; 
    $qry = "SELECT * FROM `users` WHERE `id`='".mysqli_escape_string($con, $userid) ."'";
    $result = mysqli_query($con, $qry);
    $member = mysqli_fetch_assoc($result);
    //echo $member;
    if($member['is_faculty']==1)
    {

    ?>

  <div class="container" style="margin-top:30px">
    <div class="page-heading">
        <div class="col-md-3">
        <b><h2>ANNOUNCEMENTS</h2></b>
        </div>
        <div class="col-md-3">
        <div class="pull-right"><a class="logout" href="logout.php">Logout</a></div>
        </div>
    </div>
 
   <div class="container">
      <div class="col-md-12">
        <h4 class="page-header">Edit/Remove Announcements</h4>
                <table class="table">
                <thead>
                <tr>
                  <th>Sr No</th>
                  <th>Title</th>
                  <th>Description</th>
                  <!-- <th>Course</th> -->
                  <th>Delete</th>
                  <th>Edit</th>
                  </tr>
                </thead>
               
      <?php
      include 'api/get-news.php';
      $count=1;
      foreach ($news as $new) {
      
      ?>
      <tr>
      <td><?php echo $count;?></td>
      <td><?php echo $new['title']?></td>
      <td><?php echo $new['description']?></td>
     <!--  <td><?php echo $new['course']?></td> -->
      <td><a href="delete-announcement.php?id=<?php echo $new['id'] ?>">Delete</a></td>
      <td><a href="edit-announcement.php?id=<?php echo $new['id'] ?>">Edit</a></td>
      
    
      </tr>
    <?php
    $count++;
    }
    ?>
     </table>
    </div>
    <hr>
    <div class="col-xs-12" style="text-align:center;"><a href="add-announcement.php">Add New Announcement</a></div>

    <div class="col-xs-12" style="text-align:center;"><a href="home.php">Back to home</a></div>
  </div>
 <?php
}
else{

    header("location: access-denied.php");
    exit();
}

 ?>
   
  </body>
  <script>
  $(".collapse").collapse('hide');
  $(".accordion").click(function() {
    $(".collapse").collapse('hide');
    $(this).children().children().last().collapse('toggle');
  });
  </script>
  <style type="text/css">
  .accordion {
    border 1px;
  }
  .accordion{
    background-color:#FFFFF0;
  }

  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
</html>