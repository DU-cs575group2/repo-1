<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
<?php include 'config.php'; ?>
  <title>Post</title>  
  </head>
  <body>
  	<?php 
    $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
    $db = mysqli_select_db($con, DB_DATABASE);
  	$var = 'SELECT * FROM posts WHERE id='.$_GET['id'].'';
    $result1 = mysqli_query($con, $var);
    $post = mysqli_fetch_assoc($result1);
    $post_id = $post['id'];
    $views = $post['views']+1;
    $update = "UPDATE posts SET views='$views' WHERE id='$post_id'";
    $result2 = mysqli_query($con, $update);
    
  	?>
    <div class="container">
      <div class="page-heading">
          <div class="col-md-3">
          <b><h2>POST</h2></b>
          </div>
          <div class="col-md-3">
          <div class="pull-right"><a class="logout" href="logout.php">Logout</a></div>
          </div>
      </div>
    </div>
    <div class="container">
	    <div class="heading">
	    	<h3><?php echo $post['title'];?></h3>
	    </div>
      <?php 
      if($post['user_id']==$_SESSION['SESS_USER_ID']) {  
      ?>
      <a href="delete-post.php?id=<?php echo $post['id'];?>" style="position:relative;left:270px;top:-30px;">DELETE</a>
      <?php
      }
      ?>
	    <br>
	    <div class="description">
	    	<h5><?php echo nl2br($post['description']);?></h5>
	    </div>
	    <br>
	    <div class="comments">
      <?php
  		$qry = "SELECT * FROM comments WHERE post_id = ".$post['id']." ORDER BY id ASC";
  		$result2 = mysqli_query($con, $qry);
  		$comments = array();
  		if($result2) {
  			while($row = mysqli_fetch_array($result2, MYSQLI_ASSOC)) {             
      		$comments[] = $row;
    		}
        if($_SERVER['REQUEST_METHOD'] == 'COMMENT') {
        header('Content-type: application/javascript');
        echo json_encode(array('data' => $comments));
        exit();
        }
      }
    	foreach ($comments as $comment) {
        $user = 'SELECT * FROM users WHERE id='.$comment['user_id'].'';
        $user_result = mysqli_query($con, $user);
        $member = mysqli_fetch_assoc($user_result);
    		?>
  			<div>
  				<?php 
          $content = trim($comment['content'], " ");
          echo nl2br($content);?> - <a href="profile.php?id=<?php echo $member['id'];?>"><?php echo $member['first_name']?></a>
          <?php if($comment['user_id']==$_SESSION['SESS_USER_ID'] || $post['user_id']==$_SESSION['SESS_USER_ID']) {  ?>
          <a href="delete-comment.php?id=<?php echo $comment['id'];?>" style="position:absolute;right:0;">DELETE</a>
          <?php
            }
          ?>
        </div>
        <?php 
      }
      ?>
	    </div>
	    <br>
	    <div class="write-a-comment">
	    <form class="add-comment-form" method="post" action="api/add-comment.php?id=<?php echo $post['id']; ?>">
        <div class="form-group">
          <input name="comment_description" type="text" class="form-control" placeholder="Enter comment here">
        </div>
        <p class="text-right"><button class="btn btn-primary">Add comment</button></p>
			</form>
	    </div>
	    <br>
      <div class="col-xs-12" style="text-align:center;"><a href="forum.php">Back to Forum</a></div>
    </div>
  </body>
  <style type="text/css">
  	.page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
</html>