<?php include 'auth.php';
include 'config.php';
include 'header.php';
include 'navbar.php';
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
$qry = "SELECT * FROM courses";
$result = mysqli_query($con, $qry);
// $courses = mysqli_fetch_assoc($result);
?>
<title>Select Course</title>  
  </head>
  <body>
  	<div class="container">
  		<div class="col-md-12">
	   		<div class="col-md-4"></div>
	  		<div class="col-md-4">
	  			<h3>Select Courses</h3>
	  			<form onsubmit="return false;" method="post" name="select-course-form" id="select-course-form">
	  				<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['SESS_USER_ID'];?>">
	  				<h2>Course 1</h2>
	  				<select class="form-control" id="course1" name="course1">
                          <option value=""></option>
                          <?php
                            $sqly="SELECT `id`, `course_name` from `courses`";
                            $result=mysqli_query($con, $sqly);
                            $count=mysqli_num_rows($result);
                            $c=0;
                            while($rows=mysqli_fetch_array($result)){
                            $c++;
                             ?>   
                             <option value="<?php echo $rows['id']?>"><?php echo $rows['course_name']?></option>  
                             <?php
                            }


                            ?>
                    </select>
                    <h2>Course 2</h2>
	  				<select class="form-control" id="course2" name="course2">
                          <option value=""></option>
                          <?php
                            $sqly="SELECT `id`, `course_name` from `courses`";
                            $result=mysqli_query($con, $sqly);
                            $count=mysqli_num_rows($result);
                            $c=0;
                            while($rows=mysqli_fetch_array($result)){
                            $c++;
                             ?>   
                             <option value="<?php echo $rows['id']?>"><?php echo $rows['course_name']?></option>  
                             <?php
                            }?>
                    </select>
                    <h2>Course 3</h2>
	  				<select class="form-control" id="course3" name="course3">
                          <option value=""></option>
                          <?php
                            $sqly="SELECT `id`, `course_name` from `courses`";
                            $result=mysqli_query($con, $sqly);
                            $count=mysqli_num_rows($result);
                            $c=0;
                            while($rows=mysqli_fetch_array($result)){
                            $c++;
                             ?>   
                             <option value="<?php echo $rows['id']?>"><?php echo $rows['course_name']?></option>  
                             <?php
                            }
                            ?>
                    </select>
                    <br>
                    <button type="submit" class="btn btn-primary" onclick="checkForm();">Submit</button>
	  			</form>	
	  		</div>	
	  		<div class="col-md-4"></div>		
  		</div>
  	</div>
  </body>
</html>


<script type="text/javascript">
     
   
function checkForm() {


course1 = document.getElementById("course1").value;
course2 = document.getElementById("course2").value;
course3 = document.getElementById("course3").value;



if (course1 == "" && course2 == "" && course3 == "") {

alert("Select atleast 1 course");

return false;

}
if ((course1 == course2 && course1!="")||(course2 == course3 && course2!="")||(course3 == course1 && course!="")) {

alert("Select unique courses");

return false;

}
senddata();
return true;

}


     function senddata()
    {

       // e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: 'api/select-course.php',
                    data: $('#select-course-form').serialize(),
                    success: function(data) {
                    if (data.done){
                        alert('You have enrolled for the courses');
                            document.location = 'home.php';
                    }

                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
                });

    }
    </script>