<?php include 'auth.php'; ?>
<?php include 'config.php'; ?>
<?php 
  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
  mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
  $qry = "SELECT * FROM users WHERE email='".$_SESSION['SESS_EMAIL']."'";
  $result = mysqli_query($con, $qry);
  $member = mysqli_fetch_assoc($result);
  if($member['first_login']=='0')
  {
?>
<?php include 'header.php'; ?>
  <title>Profile Details</title>  
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="about.php">VAlumni</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav pull-right">
            <li>
              <a href="#"><?php echo 'Hello, '.$_SESSION['SESS_NAME']; ?>!</a>
            </li>
            <li>
              <a href="logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <h1>Profile</h1>
      </div>
    </div>
    <div class="container">
      <form id="profile" action="api/profile-detail-form.php" method="POST">
        <div class="form-group">
          <label class="control-label">Date of Birth</label>
          <div class="controls">
            <input name="dob" type="text" class="form-control" placeholder="dd-mm-yyyy">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label">Gender</label>
          <select name="gender" class="form-control">
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
        <div class="form-group">
          <label class="control-label">Address</label>
          <div class="controls">
            <textarea name="address" class="form-control"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label">Contact Number</label>
          <div class="controls">
            <input name="contact" type="text" class="form-control">
          </div>
        </div>
        <button class="btn btn-primary">Submit</button>
      </form>
    </div>
    <script>
    $(function () {
      $('#profile').submit(function(event) {
        $(this).find('.form-group').removeClass('has-error');
        var err = false;
        var formData = $(this).serializeArray();
        for(data in formData) {
          if(formData[data].value.length < 1) {
            $('[name='+formData[data].name+']').parents('.form-group').addClass('has-error');
            err = true;
          }
        }
        if(!err)
          return true;
        else
          return false;
      });
    });
    </script>
    <?php
    }
    else if($member['first_login']=='1')
    {
      header("location: home.php");
    }
    ?>
  </body>
</html>