<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
  <title>Announcements</title>  
  <style>
.alert{
    margin:10px 0 0px 0;
}
</style>
  </head>
  <body>
  <?php include 'navbar.php'; 
  require_once 'config.php';
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysqli_select_db($link, DB_DATABASE) or die("cannot select DB");
$id=$_SESSION['SESS_USER_ID'];
$sqlx="SELECT * from `users` WHERE `id`='".mysqli_escape_string($link, $id)."';";
//echo $sqlx;exit;
$resultx=mysqli_query($link, $sqlx);
$value1 = mysqli_fetch_assoc($resultx);
//echo $value1;exit;
if($value1['is_faculty']=='1')
{
  if (isset($_GET['id']))
  {
    $ann_id=$_GET['id'];
    $sqly="SELECT * from news WHERE id='".mysqli_escape_string($link, $ann_id)."';";
    $resulty=mysqli_query($link, $sqly);
    $value2 = mysqli_fetch_assoc($resulty);

    if($value2['user_id']==$id)
    {
      ?>
         <div class="container">
        <div class="row">
            <div class="col-xs-offset-3 col-md-6">
                <h4 class="page-header">Edit Announcements</h4>
                <form class="add-announcement-form" onsubmit="return false;">
                    <div class="form-group">
                        <label class="control-label">Announcement Title</label>
                        <div class="controls">
                            <input type="text" id="announcement_title" name="announcement_title" class="form-control" value="<?php echo $value2['title']?>">
                            <input type="hidden" id="ann_id" name="ann_id" value="<?php echo $ann_id;?>">
                             <div class="error" id="titleError"><div class="alert alert-danger" role="alert" >This Field is Required</div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Detail</label>
                        <textarea name="announcement_description" id="announcement_description" class="form-control"><?php echo $value2['description']?></textarea>
                         <div class="error" id="descError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Course</label>
                        <select class="form-control" id="course" name="course">
                          <?php
                             include 'config.php';
                                
                                $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
                                mysqli_select_db($con, DB_DATABASE)or die("cannot select DB");
                            $sqly="SELECT `id`, `course_name` from `courses` WHERE `faculty`='".$_SESSION['SESS_USER_ID']."'";
                            $result=mysqli_query($con, $sqly);
                            $count=mysqli_num_rows($result);
                            $c=0;
                            while($rows=mysqli_fetch_array($result)){
                            $c++;
                             ?>   
                             <option value="<?php echo $rows['id']?>"><?php echo $rows['course_name']?></option>  
                             <?php
                            }


                            ?>
                        </select>
                         
                    </div>
                    <p class="text-right"><button class="btn btn-primary" onclick="checkForm();">Edit Announcement</button>
                    </p>
                </form>
            </div>
        </div>
    </div>


      <?php

    }
    else
    { //echo "some error1";
      header("location: access-denied.php");
      exit();
    }

  }
}
else
{   //echo "error 1";
  header("location: access-denied.php");
    exit();
}
?>
 
    <script type="text/javascript">
     
    $( document ).ready(function() {
        hideAllErrors1();

});
function checkForm() {


announcement_title = document.getElementById("announcement_title").value;

announcement_description = document.getElementById("announcement_description").value;


//alert("hi");

if (announcement_title == "") {

hideAllErrors1();

document.getElementById("titleError").style.display = "inline";

document.getElementById("announcement_title").select();

document.getElementById("announcement_title").focus();

return false;

} 


else if (announcement_description == "" ) {

hideAllErrors1();

document.getElementById("descError").style.display = "inline";

document.getElementById("announcement_description").select();

document.getElementById("announcement_description").focus();

return false;

} 

senddata();



return true;

}

function hideAllErrors1() {
document.getElementById("titleError").style.display = "none"
document.getElementById("descError").style.display = "none"


}
     function senddata()
    {

       // e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: 'api/edit-announcement.php',
                    data: $('.add-announcement-form').serialize(),
                    success: function(data) {
                    if (data.done){
                        alert('Announcement has been edited');
                            document.location = 'home.php';
                    }

                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
                });

    }
    </script>
  </body>
  <script>
  $(".collapse").collapse('hide');
  $(".accordion").click(function() {
    $(".collapse").collapse('hide');
    $(this).children().children().last().collapse('toggle');
  });
  </script>
  <style type="text/css">
  .accordion {
    border 1px;
  }
  .accordion{
    background-color:#FFFFF0;
  }

  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
</html>