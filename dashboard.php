<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard</title>
</head>
<?php include 'admin-navbar.php'; ?>

    <div class="container">
      <div class="row">
        <h2>Welcome to the dashboard!</h2>
        <div class="col-md-12">
          <div class="col-md-6">
          <h5>Events</h5>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="150">Title</th>
                        <th>Description</th>
                        <th width="120">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      require_once 'config.php';
                      include './api/get-events.php';
                      $count=0;
                      foreach ($posts as $post) {
                      if($count<3){
                    ?>
                      <tr>
                          <td><?php echo $post['id']; ?></td>
                          <td><?php echo $post['title']; ?></td>
                          <td><?php echo $post['description']; ?></td>
                          <td><?php echo $post['date']; ?></td>
                      </tr>
                    <?php
                      $count++;
                      }
                      }
                    ?>
                </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-6">
          <h5>News</h5>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="150">News title</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      require_once 'config.php';
                      include './api/get-news.php';
                      $count=0;
                      foreach ($posts as $post) {
                      if($count<3){
                    ?>
                      <tr>
                          <td><?php echo $post['id']; ?></td>
                          <td><?php echo $post['title']; ?></td>
                          <td><?php echo $post['description']; ?></td>
                      </tr>
                    <?php   
                      $count++;
                      } 
                      }
                    ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>