<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
<?php include 'config.php'; ?>
<?php include 'navbar.php'; ?>
  <?php 
  $user = $_GET['id'];
  if(!$user)
  {
    $user = $_SESSION['SESS_USER_ID'];
  }
  ?>
  <title>Profile - <?php echo $_SESSION['SESS_NAME'];?></title>  
  </head>
  <body>
  <?php include 'navbar.php'; ?>
    <div class="container">
      <div class="col-xs-12"><hr></div>
      <div class="col-xs-12">
        <?php 
        $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
        mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
        $qry = "SELECT * FROM users WHERE id ='$user'";
        $result = mysqli_query($con, $qry);
        $member = mysqli_fetch_assoc($result);
        ?>
        <h3><b>NAME: </b>
        <?php
        echo $member['first_name']." ".$member['last_name'];
        ?>
        <br>
        <b>GENDER: </b>
        <?php
        echo $member['gender'];
        ?>
        <br>
        <b>UNIVERSITY ID </b>
        <?php
        echo $member['univ_id'];
        ?>
        <br>
        <b>EMAIL ADDRESS: </b>
        <?php
        echo $member['email'];?>
        <br>
        <b>CONTACT NUMBER: </b>
        <?php
        echo $member['mobile_no'];
        ?>
        <br>
        <b>ADDRESS: </b>
        <?php
        echo $member['address']
        ?>
        <br>
        <b>DATE OF BIRTH: </b>
        <?php
        $birthday = date("d-m-Y", strtotime($member['birthday']));
        echo $birthday;
        ?>
        <br>
        <b>BRANCH: </b>
        <?php
        echo $member['branch'];
        ?>
        <br>
        <b>PROFILE TYPE: </b>
        <?php
        if($member['is_admin']==1){
          echo "Admin";
        }
        elseif ($member['is_faculty']==1) {
          echo "Faculty";
        }
        else{
          echo "Student";
        }
        ?>
        </h3>
        <?php 
        if($user==$_SESSION['SESS_USER_ID']) {
        ?> 
        <a href="edit-profile.php">EDIT PROFILE</a>
        <?php 
        }
        ?>
      </div>
      <div class="col-xs-12"><hr></div>
      <div class="col-xs-12" style="text-align:center;"><a href="home.php">Back to home</a></div>
    </div>
  </body>
  <style type="text/css">
  .user-image {
    max-width: 100px;
    max-height: 100px;
  }

  .space {
    margin-top: 30px;
  }

  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
            
</html>
