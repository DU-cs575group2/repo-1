<?php include 'auth.php'; ?>
<?php include 'header.php'; 
require_once 'config.php';
  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
  $db = mysqli_select_db($con, DB_DATABASE);
?>

  <title>Notes</title>  
  <style>
.alert{
    margin:10px 0 0px 0;
}
</style>
  <script type="text/javascript">
     

$(document).ready(function() { 
   hideAllErrors1();
    var options = { target: '#output' }; 
    $('#add-note-form').submit(function() { 
        $(this).ajaxSubmit(options); 
        return false; 
    });
}); 

/*function checkForm() {


note_title = document.getElementById("note_title").value;

note_description = document.getElementById("note_description").value;


//alert("hi");

if (note_title == "") {

hideAllErrors1();

document.getElementById("titleError").style.display = "inline";

document.getElementById("note_title").select();

document.getElementById("note_title").focus();

return false;

} 


else if (note_description == "" ) {

hideAllErrors1();

document.getElementById("descError").style.display = "inline";

document.getElementById("note_description").select();

document.getElementById("note_description").focus();

return false;

} 
else if(document.getElementById("files1").value = "") {
  
document.getElementById("fileError").style.display = "inline";

document.getElementById("files").select();

document.getElementById("files").focus();

return false;
}

$("#add-note-form").ajaxSubmit({
    
    url: 'api/add-notes.php',
    type: 'post', 
    processData: false, // Don't process the files
    contentType: false,
    success:function(data){
    if (data.done){
                        alert('Note has been uploaded');
                            document.location = 'home.php';
                    }

                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
      
    
    });



return true;

}
*/
function hideAllErrors1() {
document.getElementById("titleError").style.display = "none"
document.getElementById("descError").style.display = "none"
document.getElementById("fileError").style.display = "none"


}
  
    </script>
  </head>
  <body>
  <?php //echo $_SESSION['SESS_USER_ID'];  ?>
 <?php include 'navbar.php'; 
    $qry = "SELECT * FROM `users` WHERE `id`='".mysqli_escape_string($con, $userid) ."'";
    $result = mysqli_query($con, $qry);
    $member = mysqli_fetch_assoc($result);
    //echo $member;
    if($member['is_faculty']==1)
    {

    ?>
  
  <div class="container">
        <div class="row">
            <div class="col-xs-offset-3 col-md-6">
                <h4 class="page-header">Add Notes</h4>
                <form action="api/add-notes.php" method="post" id="add-note-form" role="alert" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label">Notes Title</label>
                        <div class="controls">
                            <input type="text" id="note_title" name="note_title" class="form-control" placeholder="Notes Title">
                             <div class="error" id="titleError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Detail</label>
                        <textarea name="note_description" id="note_description" class="form-control"></textarea>
                         <div class="error" id="descError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Course</label>
                        <select class="form-control" id="course" name="course">
                          <?php
                             include 'config.php';
                                
                                $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
                                mysqli_select_db($con, DB_DATABASE)or die("cannot select DB");
                            $sqly="SELECT `id`, `course_name` from `courses` WHERE `faculty`='".$_SESSION['SESS_USER_ID']."'";
                            $result=mysqli_query($con, $sqly);
                            $count=mysqli_num_rows($result);
                            $c=0;
                            while($rows=mysqli_fetch_array($result)){
                            $c++;
                             ?>   
                             <option value="<?php echo $rows['id']?>"><?php echo $rows['course_name']?></option>  
                             <?php
                            }


                            ?>
                        </select>
                         
                    </div>
                    <div class="form-group">
                        <label class="form-label">Upload File</label>
                        <input type="file" class="form-control" id="files1" name="files1">
                         <div class="error" id="fileError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                   
                    <p class="text-right">
                       <input type="submit" name="Submit" id="Submit" class="btn btn-primary"  value="Submit">
                   <!--  <button class="btn btn-primary" onclick="checkForm();">Add Notes</button> -->
                    </p>
                     <div id="output"></div>
                </form>
            </div>
        </div>
    </div>
  <?php
}
else{

    header("location: access-denied.php");
    exit();
}

 ?>
  </body>
  <script>
  $(".collapse").collapse('hide');
  $(".accordion").click(function() {
    $(".collapse").collapse('hide');
    $(this).children().children().last().collapse('toggle');
  });
  </script>
  <style type="text/css">
  .accordion {
    border 1px;
  }
  .accordion{
    background-color:#FFFFF0;
  }

  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
</html>