<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard - Add News</title>
</head>
<?php include 'navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-offset-3 col-md-6">
                <h4 class="page-header">Add Announcements</h4>
                <form class="add-news-form">
                    <div class="form-group">
                        <label class="control-label">Announcement Title</label>
                        <div class="controls">
                            <input type="text" name="news_title" class="form-control" placeholder="Mr. Joshi resigns as Principle">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Detail</label>
                        <textarea name="news_description" class="form-control"></textarea>
                    </div>
                    <p class="text-right"><button class="btn btn-primary">Add Announcement</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
      $(function() {
            $('.add-news-form').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: 'api/add-news.php',
                    data: $('.add-news-form').serialize(),
                    success: function(data) {
                        if (data.done)
                            document.location = 'admin-dashboard.php'
                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
                });
            });
        });
    </script>
</body>

</html>
