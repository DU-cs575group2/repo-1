<?php include 'header.php'; ?>
<title>Register</title>
</head>
<body>
	<div class="container">
    <form id="reg-form" class="form-signin" method="post" action="api/register_ac.php">
      <h3 class="text-center">Register</h3>
      <div class="form-group">
        <label class="control-label">Email</label>
        <div class="controls">
          <input type="email" class="form-control" placeholder="mogambo@india.com" name="email" maxlength="30">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label">Password</label>
        <div class="controls">
          <input type="password" class="form-control" placeholder="Greater than 6 characters" name="password">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label">Confirm Password</label>
        <div class="controls">
          <input type="password" class="form-control" placeholder="Confirm password" name="cpassword">
        </div>
      </div>
      <button class="btn btn-block btn-primary">Submit</button>
      <br>
      
    </form>
  </div>
  <script type="text/javascript">
  // $(function(){
  // 	$('#reg-form').submit(function (e) {
  // 		e.preventDefault();
  // 		$.ajax({
		//   type: 'POST',
		//   dataType: "json",
		//   url: 'api/register.php',
		//   data: $('#reg-form').serialize(),
		//   success: function( data ) {
  //       if(data.done)
  //         document.location = 'profile-detail-form.php';
  //       else {
  //         $("#reg-form .alert").remove();
  //         var errorDiv = '<div class="alert alert-danger">\
  //           <ul class="list-unstyled">\
  //           </ul>\
  //         </div>';
  //         $(errorDiv).insertAfter('#reg-form h3');
  //         for(error in data.errors) {
  //           $('#reg-form .alert ul').append("<li>"+data.errors[error]+"</li>");
  //         }
  //       }
		//   }
  // 	 });
  //   });
  // });
  </script>
  <style type="text/css">
  #reg-form {
    margin-top: 0;
  }
  </style>
</body>
</html>