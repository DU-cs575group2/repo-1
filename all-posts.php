<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
<?php include 'config.php'; ?>
  <title>posts</title>  
  </head>
  <body>
  <div class="container">
    <div class="page-heading">
        <div class="col-md-3">
        <b><h2>All Posts</h2></b>
        </div>
        <div class="col-md-3">
        <div class="pull-right"><a class="logout" href="logout.php">Logout</a></div>
        </div>
    </div>
  </div>
  <div class="container" style="text-align:center;">
    <br>
    <?php
    $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
    $db = mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");

    $qry1="SELECT * FROM posts ORDER BY id DESC";
    $result1=mysqli_query($con, $qry1);
    $posts = array();

    //Check whether the query was successful or not
    if($result1) {
    while($row = mysqli_fetch_array($result1, MYSQLI_ASSOC)) {             
        $posts[] = $row;
    }
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
      header('Content-type: application/javascript');
      echo json_encode(array('data' => $posts));
      exit();
    }
    }
    $count=count($posts);
    $start = $_GET['start'];
    $start1 = $start;
    $start2 = $start;
    ?>
    
    <a class="btn btn-primary" href="all-posts.php?start=<?php $start2=$start2-5;if($start2<=0)echo 0;else echo $start2;?>">Prev</a>
    <a class="btn btn-primary" href="all-posts.php?start=<?php $start1=$start1+5;if($start1<$count)echo $start1;else echo "0"; ?>">Next</a>
    <br>
    
    <?php
    $slice=array_slice($posts,$start,5);
    foreach ($slice as $post) {
    $user_id = $post['user_id'];
    $qry2 = "SELECT * FROM users WHERE id = $user_id";
    $result2 = mysqli_query($con, $qry2);
    $member = mysqli_fetch_assoc($result2);
    ?>
    <h4><a href='post.php?id=<?php echo $post['id']?>'><?php  echo $post['title']; ?></a> - <a href="profile.php?id=<?php echo $member['id']; ?>"><?php echo $member['first_name']; ?></a></h4>
    <?php
    }
    ?>
    <hr>
    <div class="col-xs-12" style="text-align:center;"><a href="home.php">Back to Home</a></div>
  </div>
  </body>
  <style type="text/css">
  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
  </style>
</html>
