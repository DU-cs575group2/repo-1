<?php include 'auth.php'; ?>
<?php include 'config.php'; ?>
<?php include 'header.php'; ?>
  <title>Profile Details</title>  
  </head>
  <?php
  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
  mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
  $qry = "SELECT * FROM users WHERE id='".$_SESSION['SESS_USER_ID']."'";
  $result = mysqli_query($con, $qry);
  $member = mysqli_fetch_assoc($result);
  ?>
  <body>
  <?php include 'navbar.php';?>
    <div class="container">
      <div class="col-xs-12"><hr></div>
      <div class="page-heading">
          <div class="col-md-12">
          <b><h2>EDIT-PROFILE</h2></b>
          </div>
          <br>
      </div>
      <form id="profile" action="api/edit-profile.php" method="POST" enctype="multipart/form-data">
        <div class="form-group col-md-8">
          <label class="control-label">Address</label>
          <div class="controls">
            <textarea name="address" id="address" class="form-control"><?php echo $member['address'];?></textarea>
          </div>
        </div>
        <div class="form-group col-md-8">
          <label class="control-label">Contact Number</label>
          <div class="controls">
            <input name="contact" type="text" class="form-control" value ="<?php echo $member['mobile_no'];?>">
          </div>
        </div>
        <div class="col-md-8">
          <button class="btn btn-primary">Submit</button>
        </div>
      </form>
      <hr>
      <div class="col-xs-12" style="text-align:center;"><a href="profile.php">Back to Profile</a></div>
    </div>
    <script>
    $(function () {
      document.getElementById("address").value = "Fifth Avenue, New York City";
      $('#profile').submit(function(event) {
        $(this).find('.form-group').removeClass('has-error');
        var err = false;
        var formData = $(this).serializeArray();
        for(data in formData) {
          if(formData[data] value .length < 1) {
            $('[name='+formData[data].name+']').parents('.form-group').addClass('has-error');
            err = true;
          }
        }
        if(!err)
          return true;
        else
          return false;
      });
    });
    </script>
    <style type="text/css">
    .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

    body {
            padding-top: 0px;
            padding-bottom: 0px;
        }

    .logout {
          color: white;
          position: absolute;
          bottom: 20px;
          right: 20px;
        }
    </style>
  </body>
</html>