<?php
	include_once 'auth.php';
	
	//Unset the variables stored in session
	unset($_SESSION['SESS_EMAIL']);
	unset($_SESSION['SESS_USER_ID']);
	unset($_SESSION['SESS_NAME']);
	unset($_SESSION['SESS_ADMIN']);

	session_write_close();
	header("location: index.php");
?>
