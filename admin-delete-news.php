<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard - Delete News</title>
</head>
<?php include 'admin-navbar.php'; ?>

        <div class="container">
            <div class="row">
                <div class="col-xs-offset-2 col-md-8">
                    <h4 class="page-header">Delete News</h4>
                    <form id="news-search" style="text-align:center;margin-bottom:30px;" method="GET" action="admin-delete-news.php?<?php echo $_GET["find"]; ?>">
                        <input type="text" name="find" placeholder="Search by News title">
                        <input type="submit" class="btn-primary" value="Find">
                    </form>
                    <form class="delete-news-form" method="post">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th width="150">Title</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include 'config.php';
                                $tbl_name="news";
                                $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
                                mysqli_select_db($con, DB_DATABASE)or die("cannot select DB");
                                if(isset($_GET['find'])){
                                    $find=$_GET['find'];
                                    $sql="SELECT * FROM news WHERE CONCAT(title) LIKE '%$find%'";
                                }
                                else
                                    $sql="SELECT * FROM news";
                                $result=mysqli_query($con, $sql);
                                $count=mysqli_num_rows($result);
                                $c=0;
                                while($rows=mysqli_fetch_array($result)){
                                $c++;
                                ?>
                                    <tr>
                                        <td align="center" bgcolor="#FFFFFF"><input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?php echo $rows['id']; ?>"></td>
                                        <td bgcolor="#FFFFFF"><?php echo $c; ?></td>
                                        <td bgcolor="#FFFFFF"><?php echo $rows['title']; ?></td>
                                        <td bgcolor="#FFFFFF"><?php echo $rows['description']; ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <p>
                            <input class="btn btn-primary" name="delete" type="submit" id="delete" value="Delete Selected"></td>
                        </p>
                        <?php
                        $delete = $_POST['checkbox'];

                        //Then do what you want with the selected items://
                        if($delete) {
                            foreach ($delete as $id) {

                                $query="DELETE FROM news WHERE id = '".$id."'";
                                $result1= mysqli_query($con, $query);

                            }
                        }
                        //Show that the items have been successfully removed.//
                        if($result1){
                            echo "<meta http-equiv=\"refresh\" content=\"0;URL=delete-news.php\">";
                        }

                        mysqli_close($con);
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
