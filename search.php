<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
<?php include 'config.php'; ?>
  <title>Search</title>  
  </head>
  <body>
  <div class="container">
    <div class="page-heading">
        <div class="col-md-3">
        <b><h2>SEARCH</h2></b>
        </div>
        <div class="col-md-3">
        <div class="pull-right"><a class="logout" href="logout.php">Logout</a></div>
        </div>
    </div>
  </div>
  <div class="container">
    <div class="form-holder">
    <form method= "GET" action = "result.php?name=<?php echo $_GET['name'];?>">
      <div class="form-group">
        <label class="control-label"><h4>Search by Name</h4></label>
        <div class="controls">
          <input name="name" type="text" class="form-control" id="name">
        </div>
      </div>
      <button class="btn btn-primary">Submit</button>
    </form>
    <hr>
    </div>
    <div class="form-holder">
    <form method= "GET" action = "result.php?roll_no=<?php echo $_GET['roll_no'];?>">
      <div class="form-group">
        <label class="control-label"><h4>Search by Roll No.</h4></label>
        <div class="controls">
          <input name="roll_no" type="text" class="form-control" id="roll_no">
        </div>
      </div>
      <button class="btn btn-primary">Submit</button>
    </form>
    <hr>
    </div>
    <div class="form-holder">
    <form method= "GET" action = "result.php?email=<?php echo $_GET['email'];?>">
      <div class="form-group">
        <label class="control-label"><h4>Search by Email</h4></label>
        <div class="controls">
          <input name="email" type="text" class="form-control" id="email">
        </div>
      </div>
      <button class="btn btn-primary">Submit</button>
    </form>
    <hr>
    </div>
    <div class="form-holder">
    <form method= "GET" action = "result.php?skill=<?php echo $_GET['skill'];?>">
      <div class="form-group">
        <label class="control-label"><h4>Search by Skills</h4></label>
        <div class="controls">
          <input name="skill" type="text" class="form-control" id="skill">
        </div>
      </div>
      <button class="btn btn-primary">Submit</button>
    </form>
    <hr>
    </div>
    <div class="col-xs-12" style="text-align:center;"><a href="home.php">Back to home</a></div>
  </div>
  </body>
  <style type="text/css">
  .page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }

  .form-holder {
    margin-top: 20px
  }
  </style>
</html>



