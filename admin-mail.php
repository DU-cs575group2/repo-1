<?php require 'auth.php'; ?>
<?php include 'header.php'; ?>
<title>Dashboard - Delete Post</title>
</head>
<?php include 'admin-navbar.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-xs-offset-3 col-md-6">
            <h3 class="page-heading">MAIL</h3>
            <div class="accordion" id="accordion1">
		        <div class="accordion-group">
		          	<div class="accordion-heading">
			            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
	            			<h4>Batch</h4>
			            </a>
		          	</div>
		          	<div id="" class="accordion-body collapse in">
			            <div class="accordion-inner">
		    	    		<div width="500">
		    	    			<form class="batch-mail" method="GET" action="mail.php">
				      				<div class="form-group">
	            		    <div class="controls">
	                 		  <input type="text" name="batch" class="form-control" placeholder="Write the batch here. eg: 2010">
	                		</div>
	                		</div>
	                		<div class="form-group">
	                		<div class="controls">
	                 		  <input type="subject" name="subject" class="form-control" placeholder="Write the subject here">
	                		</div>
		              		</div>
		              		<div class="form-group">
		                		<textarea name="mail_description" class="form-control" placeholder="Enter Mail Description here"></textarea>
		              		</div>
		              		<p class="text-right"><button class="btn btn-primary">Mail</button></p>
				      			</form>
		    	    		</div>
			    	    </div>
		    	  </div>
		    	</div>
		    </div>
		    <hr>
		    <div class="accordion" id="accordion1">
		        <div class="accordion-group">
		          <div class="accordion-heading">
		            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
            			<h4>Branch</h4>
		            </a>
		          </div>
		          <div id="" class="accordion-body collapse in">
		            <div class="accordion-inner">
	    	    		<div width="500">
	    	    			<form class="branch-mail" method="GET" action="mail.php">
			      				<div class="form-group">
	            		    <div class="controls">
		                 		<select name="branch" class="form-control">
							            <option value="All">All</option>
							            <option value="Information Technology">Information Technology</option>
							            <option value="Computers">Computer</option>
							            <option value="Electronics and Telecommunication">Electronics and Telecommunication</option>
							            <option value="Electronics">Electronics</option>
							            <option value="Biomedical">Biomedical</option>
						            </select>
	                		</div>
	              		</div>
	              		<div class="form-group">
                		<div class="controls">
                 		  <input type="subject" name="subject" class="form-control" placeholder="Write the subject here">
                		</div>
	              		</div>
	              		<div class="form-group">
	                		<textarea name="mail_description" class="form-control" placeholder="Enter Mail Description here"></textarea>
	              		</div>
	              		<p class="text-right"><button class="btn btn-primary">Mail</button></p>
			      			</form>
	    	    		</div>
		    	    </div>
		    	  </div>
		    	</div>
		    </div>
		    <hr>
		    <div class="accordion" id="accordion1">
		        <div class="accordion-group">
		          <div class="accordion-heading">
		            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
            			<h4>Faculty</h4>
		            </a>
		          </div>
		          <div id="" class="accordion-body collapse in">
		            <div class="accordion-inner">
	    	    		<div width="500">
	    	    			<form class="faculty-mail" method="GET" action="mail.php">
			      				<div class="form-group">
			            		    <div class="controls">
			                 		    <select name="faculty" class="form-control">
								            <option value="All">All</option>
								            <option value="Information Technology">Information Technology</option>
								            <option value="Computers">Computer</option>
								            <option value="Electronics and Telecommunication">Electronics and Telecommunication</option>
								            <option value="Electronics">Electronics</option>
								            <option value="Biomedical">Biomedical</option>
							            </select>
			                		</div>
			              		</div>
			              		<div class="form-group">
	                		<div class="controls">
	                 		  <input type="subject" name="subject" class="form-control" placeholder="Write the subject here">
	                		</div>
		              		</div>
			              		<div class="form-group">
			                		<textarea name="mail_description" class="form-control" placeholder="Enter Mail Description here"></textarea>
			              		</div>
			              		<p class="text-right"><button class="btn btn-primary">Mail</button></p>
			      			</form>
	    	    		</div>
		    	    </div>
		    	  </div>
		    	</div>
		    </div>
		    <hr>
		    <div class="accordion" id="accordion1">
		        <div class="accordion-group">
		          <div class="accordion-heading">
		            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
            			<h4>Email</h4>
		            </a>
		          </div>
		          <div id="" class="accordion-body collapse in">
		            <div class="accordion-inner">
	    	    		<div width="500">
	    	    			<form class="mail" method="GET" action="mail.php">
			      				<div class="form-group">
			            		    <div class="controls">
			                 		   <input type="text" name="email" class="form-control" placeholder="Enter only one Email address here">
			                		</div>
			              		</div>
			              		<div class="form-group">
	                		<div class="controls">
	                 		  <input type="subject" name="subject" class="form-control" placeholder="Write the subject here">
	                		</div>
		              		</div>
			              		<div class="form-group">
			                		<textarea name="mail_description" class="form-control" placeholder="Enter Mail Description here"></textarea>
			              		</div>
			              		<p class="text-right"><button class="btn btn-primary">Mail</button></p>
			      			</form>
	    	    		</div>
		    	    </div>
		    	  </div>
		    	</div>
		    </div>
		    <hr>     
        </div>
	</div>
</div>
</body>
<script type="text/javascript">
	$(".collapse").collapse('hide');
          $(".accordion-heading").click(function() {
            $(".collapse").collapse('hide');
            $(this).parent().children().last().collapse('toggle');
       	});
</script>
</html>