<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard - Add Course</title>
<style>
.alert{
    margin:10px 0 0px 0;
}
</style>
</head>
<?php include 'navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-offset-3 col-md-6">
                <h4 class="page-header">Add Course</h4>
                <form onsubmit="return false;" class="add-course-form">
                    <div class="form-group">
                        <label class="control-label">Course Name</label>
                        <div class="controls">
                            <input type="text" id="course_name" name="course_name"  class="form-control" placeholder="Course Name">
                             <div class="error" id="nameError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Course ID</label>
                        <input type="text" id="course_id" name="course_id" class="form-control" placeholder="Course ID">
                        <div class="error" id="idError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Course Description</label>
                        <div class="controls">
                         <textarea id="description" name="description" class="form-control"></textarea>
                         <div class="error" id="descError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Course Faculty</label>
                        <select class="form-control" id="faculty" name="faculty">
                        <?php
                             include 'config.php';
                                
                                $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
                                mysqli_select_db($con, DB_DATABASE)or die("cannot select DB");
                            $sqly="SELECT `univ_id`, `first_name`, `last_name` from `users` WHERE `is_faculty`='1'";
                                                       $result=mysqli_query($con, $sqly);
                            $count=mysqli_num_rows($result);
                            $c=0;
                            while($rows=mysqli_fetch_array($result)){
                            $c++;
                             ?>   
                             <option value="<?php echo $rows['id']?>"><?php echo $rows['first_name']?>&nbsp;<?php echo $rows['last_name']?></option>  
                             <?php
                            }


                            ?>
                         
                              
                          </select>
                          <div class="error" id="facultyError"><div class="alert alert-danger" role="alert">This Field is Required</div></div>
                    </div>
                    
                    
                    <p class="text-right"><button class="btn btn-primary" onclick="checkForm();">Add Course</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $( document ).ready(function() {
        hideAllErrors1();

});
function checkForm() {


course_name = document.getElementById("course_name").value;

course_id = document.getElementById("course_id").value;

description = document.getElementById("description").value;

faculty = document.getElementById("faculty").value; 

//alert("hi");

if (course_name == "") {

hideAllErrors1();

document.getElementById("nameError").style.display = "inline";

document.getElementById("course_name").select();

document.getElementById("course_name").focus();

return false;

} 


else if (course_id == "" ) {

hideAllErrors1();

document.getElementById("idError").style.display = "inline";

document.getElementById("course_id").select();

document.getElementById("course_id").focus();

return false;

} 


else if (description == "" ) {

hideAllErrors1();

document.getElementById("descError").style.display = "inline";

document.getElementById("description").select();

document.getElementById("description").focus();

return false;

}

else if (faculty == "" ) {

hideAllErrors1();

document.getElementById("facultyError").style.display = "inline";

document.getElementById("faculty").select();

document.getElementById("faculty").focus();

return false;

} 



senddata();



return true;

}
function hideAllErrors1() {
document.getElementById("idError").style.display = "none"
document.getElementById("descError").style.display = "none"
document.getElementById("facultyError").style.display = "none"
document.getElementById("nameError").style.display = "none"

}
    function senddata()
    {

       // e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: 'api/add-course.php',
                    data: $('.add-course-form').serialize(),
                    success: function(data) {
                    if (data.done){
                        alert('Course has been created');
                            document.location = 'admin-dashboard.php';
                    }

                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
                });

    }
     
    </script>
</body>

</html>
