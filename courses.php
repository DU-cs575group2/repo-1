<?php include 'auth.php'; ?>
<?php include 'config.php'; ?>
<?php include 'header.php'; ?>
  <title>Course</title>  
  </head>
  <?php
  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
  mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
  $qry1 = "SELECT * FROM users WHERE id='".$_SESSION['SESS_USER_ID']."'";
  $result1 = mysqli_query($con, $qry1);
  $member = mysqli_fetch_assoc($result1);
  $course1=$member['course1'];
  $course2=$member['course2'];
  $course3=$member['course3'];
  $qry2 = "SELECT * FROM courses WHERE id='".$course1."'";
  $qry3 = "SELECT * FROM courses WHERE id='".$course2."'";
  $qry4 = "SELECT * FROM courses WHERE id='".$course3."'";
  $result2 = mysqli_query($con, $qry2);
  $result3 = mysqli_query($con, $qry3);
  $result4 = mysqli_query($con, $qry4); 
  $value1 = mysqli_fetch_assoc($result2);
  $value2 = mysqli_fetch_assoc($result3);
  $value3 = mysqli_fetch_assoc($result4);
  $faculty1 = $value1['faculty'];
  $faculty2 = $value2['faculty'];
  $faculty3 = $value3['faculty'];
  $qry5 = "SELECT * FROM users WHERE id='".$faculty1."'";
  $qry6 = "SELECT * FROM users WHERE id='".$faculty2."'";
  $qry7 = "SELECT * FROM users WHERE id='".$faculty3."'";
  $result5 = mysqli_query($con, $qry5);
  $result6 = mysqli_query($con, $qry6);
  $result7 = mysqli_query($con, $qry7);
  $value4 = mysqli_fetch_assoc($result5);
  $value5 = mysqli_fetch_assoc($result6);
  $value6 = mysqli_fetch_assoc($result7);
  ?>
  <body>
  	<div class="container">
 		<div class="col-md-12 col-lg-12 home-content">
        <!-- <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview</a></li>
            <li><a href="#">Announcements</a></li>
            <li><a href="#">Notes</a></li>
            <li><a href="#">Instructor</a></li>
          <?php if($member['is_faculty']=='1'){ ?><li><a href="#">Student List</a></li><?php } ?> 
          </ul>
        </div> -->
        <div class="col-sm-12 col-md-12 offset-md-2 main">
          <?php if ($member['course1'] || $member['course2'] || $member['course3']){ ?>
          <h1>Courses</h1>

          <h2>Section title</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Course Name</th>
                  <th>Course ID</th>
                  <th>Professor</th>
                  <th>Description</th>
                </tr>
              </thead>
              <?php if($course1!=null){?>
              <tbody>
                <tr>
                  <td>1</td>
                  <td><a href="overview.php?courseid=<?php echo $value1['id'];?>"><?php echo $value1['course_name'];?></a></td>
                  <td><?php echo $value1['course_id'];?></td>
                  <td><?php echo $value4['first_name']?>&nbsp;<?php echo $value4['last_name']?></td>
                  <td><?php echo $value1['description'];?></td>
                </tr>
              </tbody>
              <?php }?>
              <?php if($course2!=null){?>
              <tbody>
                <tr>
                  <td>2</td>
                  <td><a href="overview.php?courseid=<?php echo $value2['id'];?>"><?php echo $value2['course_name'];?></a></td>
                  <td><?php echo $value2['course_id'];?></td>
                  <td><?php echo $value5['first_name']?>&nbsp;<?php echo $value5['last_name']?></td>
                  <td><?php echo $value2['description'];?></td>
                </tr>
              </tbody>
              <?php }?>
              <?php if($course3!=null){?>
              <tbody>
                <tr>
                  <td>3</td>
                  <td><a href="overview.php?courseid=<?php echo $value3['id'];?>"><?php echo $value3['course_name'];?></a></td>
                  <td><?php echo $value3['course_id'];?></td>
                  <td><?php echo $value6['first_name']?>&nbsp;<?php echo $value6['last_name']?></td>
                  <td><?php echo $value3['description'];?></td>
                </tr>
              </tbody>
              <?php }?>
            </table>
          </div>
          <?php } else {?>

          <h4>You haven't selected any courses yet. Please Select your courses by clicking the button below.</h4>
          <a href="select-course.php"><button class="btn-primary">Click Here</button></a>


          <?php }?>
        </div> 
        </div>
        

  	</div>
  </body>
  <?php include 'navbar.php'; ?>
  <style type="text/css">
  		body {
	        padding-top: 0px;
	        padding-bottom: 0px;
  		}
		.home-option {
			padding: 25px 0px;
			text-align: center;
			background-color: #008cba;
			color: white;
		}
		.home-option:active {
			background-color: #009fba;
			text-decoration: none;
		}

		.home-option:hover {
			text-decoration: none;
		}	

		a {
			text-decoration: none !important;
		}

		.container {
			/*width: 100%;*/
			padding-right: 0;
			padding-left: 0;
		}

		.fullwidth {
			padding-left: 0;
			padding-right: 0;
		}

		hr {
			margin-top: 0px;
			margin-bottom: 0px;
		}
  </style>
</html>