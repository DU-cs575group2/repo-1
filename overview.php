<?php include 'auth.php'; ?>
<?php include 'config.php'; ?>
<?php include 'header.php'; ?>
  <title>Course</title>  
  </head>
  <?php
  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
  mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
  $qry1 = "SELECT * FROM users WHERE id='".$_SESSION['SESS_USER_ID']."'";
  $result1 = mysqli_query($con, $qry1);
  $member = mysqli_fetch_assoc($result1);
  $courseid = $_GET['courseid'];
  $qry2 = "SELECT * FROM courses WHERE id='".$courseid."'";
  $result2 = mysqli_query($con, $qry2);
  $value1 = mysqli_fetch_assoc($result2);
  ?>
  <body>
  	<div class="container">
 		<div class="col-md-12 col-lg-12 home-content">
	        <div class="col-sm-3 col-md-2 sidebar">
	          	<ul class="nav nav-sidebar">
		            <!-- <li class="active"><a href="overview.php?courseid=<?php echo $courseid;?>">Overview</a></li>
		            <li><a href="#">Announcements</a></li>
		            <li><a href="#">Notes</a></li> -->
		            <li><a href="profile.php?id=<?php echo $value1['faculty'];?>">Instructor</a></li>
					<!-- <?php if($member['is_faculty']=='1'){ ?><li><a href="#">Student List</a></li><?php } ?> -->
	          	</ul>
	        </div>
	        <div class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 main">
	          	<h1><?php echo $value1['course_name'];?></h1>
	          	<h2>Overview</h2>
	          	<br>
	          	<?php echo $value1['description'];?>
	        </div>
        </div>
   	</div>
  </body>
  <?php include 'navbar.php'; ?>
  <style type="text/css">
  		body {
	        padding-top: 0px;
	        padding-bottom: 0px;
  		}
		.home-option {
			padding: 25px 0px;
			text-align: center;
			background-color: #008cba;
			color: white;
		}
		.home-option:active {
			background-color: #009fba;
			text-decoration: none;
		}

		.home-option:hover {
			text-decoration: none;
		}	

		a {
			text-decoration: none !important;
		}

		.container {
			/*width: 100%;*/
			padding-right: 0;
			padding-left: 0;
		}

		.fullwidth {
			padding-left: 0;
			padding-right: 0;
		}

		hr {
			margin-top: 0px;
			margin-bottom: 0px;
		}
  </style>
</html>