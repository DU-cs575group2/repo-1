<?php include 'auth.php'; ?>
<?php include 'config.php'; ?>
<?php include 'header.php';
$user = $_SESSION['SESS_USER_ID'];
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
$qry = "SELECT * FROM users WHERE id ='$user'";
$result = mysqli_query($con, $qry);
$member = mysqli_fetch_assoc($result);?>

  <title>Home</title>
  </head>
  <body>
  <?php include 'navbar.php'; ?>
  	<div class="container">
  	<?php
  		if(!($member['is_faculty']==1))
  		{
  	  ?>
 		<div class="col-md-12 col-lg-12 home-content">
            <div class="col-xs-6 col-lg-3">
              <h2>Courses</h2>
              <p>
		       	<UL TYPE="square">
					<?php if ($member['course1']){?>
						<LI><?php
							$course=$member['course1'];
							$qry1 = "SELECT * FROM courses WHERE id = '$course'";
							$result1=mysqli_query($con, $qry1);
							$course_detail=mysqli_fetch_assoc($result1);
							echo $course_detail['course_name'];
						}?>
					<?php if ($member['course2']){?>
						<LI><?php
							$course=$member['course2'];
							$qry1 = "SELECT * FROM courses WHERE id = '$course'";
							$result1=mysqli_query($con, $qry1);
							$course_detail=mysqli_fetch_assoc($result1);
							echo $course_detail['course_name'];
						}?>
					<?php if ($member['course3']){?>
						<LI><?php
							$course=$member['course3'];
							$qry1 = "SELECT * FROM courses WHERE id = '$course'";
							$result1=mysqli_query($con, $qry1);
							$course_detail=mysqli_fetch_assoc($result1);
							echo $course_detail['course_name'];
						}?>
				</UL>
              </p>
              <p><a class="btn btn-secondary" href="courses.php" role="button">View details »</a></p>
            </div><!--/span-->
             <div class="col-xs-6 col-lg-3">
              <h2>Notes</h2>
              <p>View course notes shared by professors here </p>
              <p><a class="btn btn-secondary" href="get-notes.php" role="button">View details »</a></p>
            </div><!--/span-->
            <div class="col-xs-6 col-lg-3">
              <h2>Announcements</h2>
              <p>View course announcements made by professors here </p>
              <p><a class="btn btn-secondary" href="get-announcements.php" role="button">View details »</a></p>
            </div><!--/span-->
            <div class="col-xs-6 col-lg-3">
              <h2>Profile</h2>
              <p>
              	<h3>NAME: <?php echo $member['first_name'];?> <?php echo $member['last_name'];?> </h3>
              	<h3>STUDENT ID: <?php echo $member['univ_id'];?> 
              </p>
              <p><a class="btn btn-secondary" href="profile.php" role="button">View details »</a></p>
            </div><!--/span-->
           
          </div>
          <?php
          	}
          ?>
          <?php
  		if($member['is_faculty']==1)
  		{
  	  ?>
 		<div class="col-md-12 col-lg-12 home-content">
            <div class="col-xs-6 col-lg-4">
              <h2>Notes</h2>
              <p>Upload Notes or Documents to share with students</p>
              <p><a class="btn btn-secondary" href="notes.php" role="button">View details »</a></p>
            </div><!--/span-->
            <div class="col-xs-6 col-lg-4">
              <h2>Announcements</h2>
              <p>Create an announcement for all students enrolled for course</p>
              <p><a class="btn btn-secondary" href="announcement.php" role="button">View details »</a></p>
            </div><!--/span-->
            <div class="col-xs-6 col-lg-4">
              <h2>Profile</h2>
              <p>
              	<h3>NAME: <?php echo $member['first_name'];?> <?php echo $member['last_name'];?> </h3>
              	<h3>STUDENT ID: <?php echo $member['univ_id'];?> 
              </p>
              <p><a class="btn btn-secondary" href="profile.php" role="button">View details »</a></p>
            </div><!--/span-->
           
          </div>
          <?php
          	}
          ?>
  	</div>
  </body>
  <style type="text/css">
  		body {
	        padding-top: 0px;
	        padding-bottom: 0px;
  		}
		.home-option {
			padding: 25px 0px;
			text-align: center;
			background-color: #008cba;
			color: white;
		}
		.home-option:active {
			background-color: #009fba;
			text-decoration: none;
		}

		.home-option:hover {
			text-decoration: none;
		}	

		a {
			text-decoration: none !important;
		}

		.container {
			/*width: 100%;*/
			padding-right: 0;
			padding-left: 0;
		}

		.fullwidth {
			padding-left: 0;
			padding-right: 0;
		}

		hr {
			margin-top: 0px;
			margin-bottom: 0px;
		}
  </style>
</html>