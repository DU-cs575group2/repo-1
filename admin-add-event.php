<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard - Add Event</title>
</head>
<?php include 'admin-navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-offset-3 col-md-6">
                <h4 class="page-header">Add event</h4>
                <form class="add-event-form">
                    <div class="form-group">
                        <label class="control-label">Event Title</label>
                        <div class="controls">
                            <input type="text" name="event_title" class="form-control" placeholder="Reunion for 2014 batch">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Event Date</label>
                        <div class="controls">
                            <input type="text" name="event_date" class="form-control" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Event description</label>
                        <textarea name="event_description" class="form-control"></textarea>
                    </div>
                    <p class="text-right"><button class="btn btn-primary">Add event</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
      $(function() {
            $('.add-event-form').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: 'api/add-event.php',
                    data: $('.add-event-form').serialize(),
                    success: function(data) {
                        if (data.done)
                            document.location = 'admin-dashboard.php'
                    },
                    error: function(a, b, c) {
                        console.log(a, b, c);
                    }
                });
            });
        });
    </script>
</body>

</html>
