<?php include 'auth.php'; ?>
<?php include 'header.php'; ?>
<?php include 'config.php'; ?>
  <title>Search Result</title>  
  </head>
  <body>
  <div class="container">
      <div class="page-heading">
          <div class="col-md-3">
          <b><h2>RESULT</h2></b>
          </div>
          <div class="col-md-3">
          <div class="pull-right"><a class="logout" href="logout.php">Logout</a></div>
          </div>
      </div>
    </div>
		<?php
	  $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
	  mysqli_select_db($con, DB_DATABASE) or die("cannot select DB");
		$name = $_GET['name'];
	  $roll_no = $_GET['roll_no'];
	  $email = $_GET['email'];
	  $skill = $_GET['skill'];
	  $members = array();
		if($name){
		$search_people = mysqli_query($con, "SELECT * FROM users 
	    WHERE CONCAT(first_name, ' ', last_name) LIKE '%$name%' OR last_name LIKE '%$name%'");
		$search = $name;
		$search_by = "Name";
		}
		if($roll_no){
		$search_people = mysqli_query($con, "SELECT * FROM users 
	    WHERE CONCAT(roll_no) LIKE '%$roll_no%'");	
		$search = $roll_no;
		$search_by = "Roll No.";
		}
		if($email){
		$search_people = mysqli_query($con, "SELECT * FROM users 
	    WHERE email LIKE '$email' OR email2 LIKE '$email'");	
		$search = $email;
		$search_by = "Email";
		}
		if($skill){
		$search_people = mysqli_query($con, "SELECT * FROM users 
	    WHERE special1 LIKE '$skill' OR special2 LIKE '$skill' OR special3 LIKE '$skill'");	
		$search = $skill;
		$search_by = "Skills";
		}
		if($search_people) {
		while($row = mysqli_fetch_array($search_people, MYSQLI_ASSOC)) {             
		    $members[] = $row;
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			header('Content-type: application/javascript');
			echo json_encode(array('data' => $members));
			exit();
		}
	}

		?>
		<div class="container">
			<h3>Here is the result of your search by <?php echo $search_by;?> for '<?php echo $search?>'</h3>
			<?php
			if($members){
				?>
			<table>
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>Roll No.</th>
					<th>Email</th>
				</thead>
				<?php
				foreach ($members as $member) {
				?>
				<tr>
					<td></td>
					<td><a href="profile.php?id=<?php echo $member['id'];?>"><?php echo $member['first_name']." ".$member['last_name'];?></a></td>
					<td><?php echo $member['roll_no'];?></td>
					<td><?php echo $member['email'];?></td>
				</tr>
				<?php
				}
				}
				else {
					echo "Your Search '".$search."' did not match";
				}
				?>
			</table>
			<div class="col-xs-12" style="text-align:center;margin-top:100px;"><a href="search.php">Back to Search Page</a></div>
		</div>
	</body>
	<style type="text/css">
		.page-heading {
        background-color: #008cba;
        padding: 5px 0;
        padding-bottom: 10px;
      }

  body {
          padding-top: 0px;
          padding-bottom: 0px;
      }

  .logout {
        color: white;
        position: absolute;
        bottom: 20px;
        right: 20px;
      }
	</style>
</html>