<body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="admin-dashboard.php">Dashboard</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="admin-add-course.php">Add Course</a>
                        </li>
                        <!-- <li>
                            <a href="#">Delete Course</a>
                        </li> -->
                        <!-- <li>
                            <a href="admin-add-news.php">Add Announcements</a>
                        </li> -->
                        <li>
                            <a href="admin-add-profile.php">Add Profile</a>
                        </li>
                       <!--  <li>
                            <a href="admin-delete-profile.php">Delete Profile</a>
                        </li> -->
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li>
                            <a href="#"><?php echo 'Hello, '.$_SESSION['SESS_NAME']; ?>!</a>
                        </li>
                        <li>
                            <a href="logout.php">Logout</a>
                        </li>
                    </ul>
                </div>
            <!--/.navbar-collapse -->
            </div>
        </div>