<?php require 'auth.php'; ?>
<?php 
  if($_SESSION['SESS_ADMIN']!=1)
  {
    header("location: access-denied.php");
  }
?>
<?php include 'header.php'; ?>
<title>Dashboard - Delete Profile</title>
</head>
<?php include 'admin-navbar.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-offset-2 col-md-8">
                <h4 class="page-header">Delete Profile</h4>
                    
                    <form class="delete-comment-form" method="post">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roll Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include 'config.php';
                                $con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
                                mysqli_select_db($con, DB_DATABASE)or die("cannot select DB");
                                if(isset($_GET['find'])){
                                    $find=$_GET['find'];
                                    $sql="SELECT * FROM users WHERE CONCAT(first_name, ' ', last_name) LIKE '%$find%' OR last_name LIKE '%$find%'";
                                }
                                else
                                    $sql="SELECT * FROM users";
                                $result=mysqli_query($con, $sql);
                                $count=mysqli_num_rows($result);
                                $user_tbl="users";
                                $c = 0;
                                while($rows=mysqli_fetch_array($result)){
                                $c++;
                                ?>
                                	<tr>
                                        <td align="center"><input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?php echo $rows['id']; ?>"></td>
                                        <td><?php echo $c; ?></td>
                                        <td><?php echo $rows['first_name'];?> <?php echo $rows['last_name']; ?></td>
                                        <td><?php echo $rows['email']; ?></td>
                                        <td><?php echo $rows['univ_id']; ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <p>
                            <input class="btn btn-primary" name="delete" type="submit" id="delete" value="Delete Selected"></td>
                        </p>
                        <?php
                        $delete = $_POST['checkbox'];

                        //Then do what you want with the selected items://
                        if($delete) {
                            foreach ($delete as $id) {

                               // $query1="DELETE FROM users WHERE id = '".$id."'";
                                $query1="UPDATE `users` SET `flag` = '0' WHERE `users`.`id` = '".$id."'";
                                $query2="UPDATE `news` SET `flag` = '0' WHERE `news`.`user_id` = '".$id."'";
                                $query3="UPDATE `notes` SET `flag` = '0' WHERE `notes`.`user_id` = '".$id."'";
                                $query4="UPDATE `courses` SET `flag` = '0' WHERE `courses`.`faculty` = '".$id."'";
                               	
                                $result1= mysqli_query($con, $query1);
                                $result2= mysqli_query($con, $query2);
                                $result3= mysqli_query($con, $query3);
                                $result4= mysqli_query($con, $query4);

                            }
                        }
                        if($result1){
                            echo "<meta http-equiv=\"refresh\" content=\"0;URL=admin-delete-profile.php\">";
                        }
                        mysqli_close($con);
                        ?>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(function() {
            
        });
    </script>
    </body>
</html>